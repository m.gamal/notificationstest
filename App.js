import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {
  Notification, NotificationOpen, notifications, messaging,
} from 'react-native-firebase';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

export default class App extends Component {
  componentDidMount() {
    this.checkPermissions();
    this.FcmNotificationHandler();
  }

  FcmNotificationHandler() {
    this.notificationListener = notifications().onNotification((notification: Notification) => {
      // Notifications received while application is open in foreground
      // Not displayed in status bar so we'll have to handle it manually

      const localNotification = new notifications.Notification({
        sound: 'default',
        show_in_foreground: true,
      })
        .setNotificationId(notification.notificationId)
        .setTitle(notification.title)
        .setSubtitle(notification.subtitle)
        .setBody(notification.body)
        .setData(notification.data)
        .android.setChannelId('dogsmap-channel')
        .android.setSmallIcon('notification')
        .android.setColor('#ff4858')
        .android.setLargeIcon('ic_launcher')
        .android.setAutoCancel(true);

      notifications().displayNotification(localNotification)
        .catch((e) => {
          // console.log(e);
        });
    });

    this.notificationOpenedListener = notifications().onNotificationOpened((notificationOpen: NotificationOpen) => {
      // Notification opened when the app is in background
      const action = notificationOpen.action;
      // Get information about the notification that was opened
      const notification: Notification = notificationOpen.notification;
      // Process notification
    });

    // App is initially closed
    // Check if application was opened by an FCM notification
    notifications().getInitialNotification().then((notificationOpen: NotificationOpen) => {
      if (notificationOpen) {
        // App was opened by a notification
      }
    });
  }

  checkPermissions() {
    // Check fcm permissions
    messaging().hasPermission().then((enabled) => {
      if (enabled) {
        // Fcm notifications are permitted by user
        this.updateFcmToken();
      } else {
        this.requestFCMPermession();
      }
    });
  }

  requestFCMPermession() {
    messaging().requestPermission().then(() => {
      // Permissions granted
      this.updateFcmToken();
    }).catch(() => {
      // User has rejected permissions
    });
  }

  updateFcmToken() {
    messaging().getToken()
      .then((token) => {
        console.log(token);
      })
      .catch((err) => {
        // console.log(err);
      });
  }

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
